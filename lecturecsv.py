#!/usr/bin/python
# -*- coding: utf-8 -*-

import pandas as pnd


#year;month;day;title;space;eduction;sf;science;seti;everythings

def index_sujet(df,score):
  nb=0
  nbseti=0
  nbedu=0
  nbspace=0
  nbsf=0
  nbscience=0
  
  for i in df.index:
    note=0
    sujet = ""
    annee = df.values[i][0]
    mois = int(df.values[i][1])
    jour = df.values[i][2]
    date = str(mois)+"/"+str(annee)
    titre = df.values[i][3]

#space(4);		+32
    if (df.values[i][4] == 1):
       note +=32
       nbspace +=1
       sujet += "SPACE "

#eduction(5);		+16
    if (df.values[i][5] == 1):
       note +=16
       nbedu +=1
       sujet += "EDU "

#sf(6);			+8
    if (df.values[i][6] == 1):
       note +=8
       nbsf +=1
       sujet += "SF "

#science(7)		+4
    if (df.values[i][7] == 1):
       note +=4
       nbscience +=1
       sujet += "SCIENCE "
   
#seti(8)		+2
    if (df.values[i][8] == 1):
       note +=2
       nbseti +=1
       sujet += "SETI "

#everythings(9)		+1
    if (df.values[i][9] == 1):
       note +=1
       sujet += "REST "
    
    #print (sujet + ": " +str(note))
    if (note == score):
       nb += 1
       print(titre)
  return nb


#Returns a table with comibination of topics
#32 Space 
#48 Space Education
#2 SETI 
#4 Science 
#10 SETI SF
#34 Space SETI
#18 Education SETI

def combinaison():
   #topics = {'Everything':1, 'SETI':2,  'Science':4, 'SF':8, 'Education':16, 'Space':32}
  topics = {'SETI':2,  'Science':4, 'SF':8, 'Education':16, 'Space':32}
  
  distrib = {}
  for t,p in topics.items():
      for t1,p1 in topics.items():
         indice = p
         label = t+" "
         if (t != t1):
            indice += p1
            label += t1
         distrib[indice] = label
  return distrib
   
combi=combinaison()
#for indice in combi:
#Si on veut que SETI
for indice in (10,34,18):
  nbtotal = 0
  print ("-------------------------------")
  print (combi[indice]) 
  for annee in range (2006,2019):
     fichier="data/post_"+str(annee)+".csv"
     #Lecture du fichier
     cy= pnd.read_csv(fichier,sep=';',header=1)
     total=index_sujet(cy,indice)
     nbtotal += total
  print (str(indice)+" : "+str(nbtotal))
