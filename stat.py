#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pnd
import numpy as np


def moyenne(tableau,tag):
    #calcule de la moyenne
    moyenne = sum(tableau)/len(tableau)
    print ("Moyenne "+tag+" : "+ str(moyenne))
    
def mediane(liste,tag):
    liste.sort
    if (len(liste)%2 == 0):
       mediane = liste[(len(liste)-1)//2]
    else:
       mediane = liste[len(liste)//2]
    print ("Médiane " + tag +" : "+str(mediane))
    
def distribution(tableau):
    v1=min(tableau)
    v2=max(tableau)+1
    nb = len(tableau)
    pas=50
    position=0
    while position < v2:
        nbpost=0
        for visite in tableau:
            difference = visite-position
            if (difference>0 and difference<pas):
                nbpost+=1
        print (str(position)+";"+str(nbpost))
        position+=pas

def histogramme(tableau):
    v1=min(tableau)
    v2=max(tableau)+1
    nb = len(tableau)
    position=0
    nbpost=0
    while position < v2:
        for visite in tableau:
            if (visite == position):
                nbpost+=100/nb
        print (str(position)+";"+str(nbpost))
        position+=1

# Le fichier contenant les stats
source="data/top.csv"
statseti="data/statseti.csv"

# Lecture du fichier
cy= pnd.read_csv(source,sep=';',header=0)
cseti= pnd.read_csv(statseti,sep=';',header=0)

# Distribution
# All posts
dist=[]

#Seti related posts
seti=[]

#Post about something else
nonseti=[]

#Post dealing only with SETI
onlyseti=[]

#Post dealing with SETI + SF
setisf=[]

#Post dealing with SETI + Education
setiedu=[]

#Post dealing with SETI + Space
setispace=[]

#Post with links to scholar papers
setischolar=[]
seticonf=[]

for i in cy.index:
    ligne=cy.values[i]
    classement=ligne[0]
    titre=ligne[1]
    nbvisites=ligne[2]
    isseti=pnd.isnull(ligne)[3]
    if (isseti):
        nonseti.append(nbvisites)
    else:
        seti.append(nbvisites)
        #Si on a envie de générer un csv
        #print("\""+titre+"\";"+str(nbvisites))
    dist.append(nbvisites)

for i in cseti.index:
    nbtopic=0
    ligne=cseti.values[i]
    titre=ligne[0]
    nbvisites=ligne[1]
    isedu=pnd.isnull(ligne)[2]
    isspace=pnd.isnull(ligne)[3]
    issf=pnd.isnull(ligne[4])
    isscholar=pnd.isnull(ligne[5])
    isconf=pnd.isnull(ligne[6])
    if (isedu == False):
        setiedu.append(nbvisites)
        nbtopic+=1
    if (isspace == False):
        setispace.append(nbvisites)
        nbtopic+=1
    if (issf == False):
        setisf.append(nbvisites)
        nbtopic+=1
    if (isscholar == False):
        setischolar.append(nbvisites)
    if (isconf == False):
        seticonf.append(nbvisites)
    if (nbtopic == 0):    
        onlyseti.append(nbvisites)
    
       
def affiche(tab,label):
    print ("----------------------")
    print (label + " : ")
    print (len(tab))  
    moyenne (tab,label)
    mediane (tab,label)
    print ("-")

#distribution(seti)
#histogramme(seti)
affiche (seti,"SETI")
affiche (nonseti,"NO SETI")
affiche (onlyseti,"ONLY SETI")
affiche (setiedu,"SETI + Edu")
affiche (setispace,"SETI + Space")
affiche (setisf,"SETI + SF")
affiche (setischolar,"Scholar SETI")   
affiche (seticonf,"Conf SETI")  
