# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 10:54:30 2018

@author: zabeth
"""

bibsource="iaa_sfls.bib"
dico=[]
FRdico=[]
isfrench = "FALSE"
nbfr=0
nb=0

with open(bibsource,"r") as bibi:
    for ligne in bibi:
        fin = ligne.find("}")
        if (fin == 0):
            isfrench = "FALSE"
            nb +=1
        test=ligne.find("Conference{FR")
        if (test == 1):
            isfrench = "TRUE"
            nbfr += 1
        titre=ligne.find("Title")
        if (titre > -1 ):            
            tmp=ligne.split('{')
            texte=tmp[1].split('}')
            mots=texte[0].split(' ')
            for quoi in mots:
                correction = quoi.lower()
                dico.append(correction)
                if (isfrench == "TRUE"):
                    FRdico.append(correction);
        
            
                
dico.sort();
old=""
print ("Titres : " + str(nb))
print ("Mots : " + str(len(dico)))
print ("French authors : " + str(nbfr))
print ("Mots : " + str(len(FRdico)))
for mot in dico:
    if (mot != old):
        combien = dico.count(mot)
        combienfr = FRdico.count(mot)
        if (combien > 1):
           print(mot + ";" + str(combien) + ";" + str(combienfr))
        old=mot

    

